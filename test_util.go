package mcbouncer

import (
	"net/http"
	"net/http/httptest"
	"path"
	"reflect"
	"strconv"
	"strings"
)

const (
	testAPIPathf          = "/api/%s/%s/%s"
	testAPIKey            = "test-api-key"
	testUser              = "notch"
	testIssuer            = "bsdlp"
	testDescription       = "some reason"
	testPage              = "0"
	testPageInt           = 0
	testResultsPerPage    = "100"
	testResultsPerPageInt = 100
	testIP                = "192.95.40.69"
	testID                = "42572"
)

func newTestServer() (server *httptest.Server) {
	testMux := http.NewServeMux()
	testMux.HandleFunc("/api/", testHTTPHandler)

	server = httptest.NewServer(testMux)
	return
}

func formatTestAPIURL(action, key string, params ...string) string {
	return path.Join("/api", action, key, path.Join(params...))
}

func testHTTPHandler(w http.ResponseWriter, r *http.Request) {
	urlParts := strings.Split(r.URL.Path, "/")[1:]
	action := urlParts[1]
	result, ok := getTestActionResponse(action)
	if !ok {
		http.Error(w, "invalid action", http.StatusNotFound)
		return
	}

	key := urlParts[2]
	if key != testAPIKey {
		http.Error(w, "invalid api key", http.StatusNotFound)
		return
	}

	var params []string
	if len(urlParts) > 3 {
		params = urlParts[3:]
	}

	matchedParams, ok := getTestActionParams(action)
	if !ok {
		http.Error(w, "invalid action", http.StatusNotFound)
		return
	}

	if !reflect.DeepEqual(params, matchedParams) {
		http.Error(w, "invalid params", http.StatusNotFound)
		return
	}

	content := []byte(result)
	w.Header().Set("Content-Length", strconv.Itoa(len(content)))
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write(content)
	if err != nil {
		http.Error(w, "can't write response", http.StatusInternalServerError)
		return
	}
	return
}

func getTestActionParams(action string) (params []string, ok bool) {
	ok = true

	switch action {
	case AddBan:
		params = []string{testIssuer, testUser, testDescription}
	case RemoveBan:
		params = []string{testUser}
	case GetBans:
		params = []string{testUser, testPage, testResultsPerPage}
	case GetBanCount:
		params = []string{testUser}
	case GetBanReason:
		params = []string{testUser}
	case GetBanDetails:
		params = []string{testUser}
	case AddIPBan:
		params = []string{testIssuer, testIP, testDescription}
	case RemoveIPBan:
		params = []string{testIP}
	case GetIPBans:
		params = []string{testIP, testPage, testResultsPerPage}
	case GetIPBanCount:
		params = []string{testIP}
	case AddNote:
		params = []string{testIssuer, testUser, testDescription}
	case RemoveNote:
		params = []string{testID}
	case GetNotes:
		params = []string{testUser, testPage, testResultsPerPage}
	case GetNoteCount:
		params = []string{testUser}
	case UpdateUser:
		params = []string{testUser, testIP}
	default:
		ok = false
	}
	return
}

func getTestActionResponse(action string) (result string, ok bool) {
	ok = true

	switch action {
	case AddBan:
		result = `{"user": "Notch", "success": true}`
	case RemoveBan:
		result = `{"username": "notch", "success": true}`
	case GetBans:
		result = `{"totalcount": 2, "data": [{"username": "Notch", "reason": "another reason", "server": "127.0.0.1:25596", "time": "2012-06-24T15:27:12", "issuer": "test1"}, {"username": "Notch", "reason": "some reason", "server": "mc.voltaire.sh", "time": "2015-12-18T10:50:08.388", "issuer": "testIssuer"}], "page": 0, "success": true}`
	case GetBanCount:
		result = `{"totalcount": 2, "success": true}`
	case GetBanReason:
		result = `{"is_banned": true, "reason": "some rason", "success": true}`
	case GetBanDetails:
		result = `{"is_banned": true, "time": "2015-12-18T10:50:08.388", "reason": "some rason", "success": true, "issuer": "bsdlp"}`
	case AddIPBan:
		result = `{"ipaddress": "192.95.40.69", "success": true}`
	case RemoveIPBan:
		result = `{"ipaddress": "192.95.40.69", "success": true}`
	case GetIPBans:
		result = `{"totalcount": 1, "data": [{"ip": "192.95.40.69", "reason": "some reason", "server": "mc.voltaire.sh", "issuer": "bsdlp"}], "page": 0, "success": true}`
	case GetIPBanCount:
		result = `{"totalcount": 1, "success": true}`
	case AddNote:
		result = `{"user": "Notch", "success": true}`
	case RemoveNote:
		result = `{"noteid": "42572", "success": true}`
	case GetNotes:
		result = `{"totalcount": 3, "data": [{"username": "Notch", "note": "some reason", "time": "2015-12-17T09:48:19.825", "issuer": "bsdlp", "noteid": 42572, "server": "mc.voltaire.sh"}, {"username": "Notch", "note": "not notch", "time": "2015-12-17T09:42:07.934", "issuer": "bsdlp", "noteid": 42571, "server": "mc.voltaire.sh"}, {"username": "Notch", "note": "1st Note", "time": "2012-02-04T13:18:17", "issuer": "youyouxue", "noteid": 6900, "server": "s.APMinecraft.com"}], "page": 0, "success": true}`
	case GetNoteCount:
		result = `{"totalcount": 2, "success": true}`
	case UpdateUser:
		result = `{"username": "notch", "success": true}`
	default:
		ok = false
	}
	return
}
