package mcbouncer

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestAPI(t *testing.T) {
	const apiURL = "http://test/%s/%s/%s"

	client := &Client{
		apiKey:  testAPIKey,
		apiURLf: apiURL,
	}

	Convey("API tests", t, func() {
		Convey("formats url correctly", func() {
			url := client.urlf("getBans", "param")
			So(url, ShouldEqual, "http://test/getBans/test-api-key/param")
		})
	})
}
