package mcbouncer

import "strconv"

// NoteResult is returned when performing actions on a note
type NoteResult struct {
	User    string `json:"user"`
	Success bool   `json:"success"`
	Error   string `json:"error"`
	ID      string `json:"noteid"`
}

// NoteDetail holds information on note
type NoteDetail struct {
	Username string `json:"username"`
	Note     string `json:"string"`
	Time     *Time  `json:"time"`
	Issuer   string `json:"issuer"`
	ID       int    `json:"noteid"`
	Server   string `json:"server"`
}

// Notes holds notes
type Notes struct {
	Count   int           `json:"totalcount"`
	Notes   []*NoteDetail `json:"data"`
	Page    int           `json:"page"`
	Success bool          `json:"success"`
}

// AddNote adds a note to player
func (c *Client) AddNote(issuer, player, note string) (result *NoteResult, err error) {
	result = &NoteResult{}
	err = c.getAndUnmarshal(result, AddNote, issuer, player, note)
	if err != nil {
		return
	}
	return
}

// RemoveNote removes a note
func (c *Client) RemoveNote(id int) (result *NoteResult, err error) {
	result = &NoteResult{}
	err = c.getAndUnmarshal(result, strconv.Itoa(id))
	if err != nil {
		return
	}
	return
}

// GetNotes retrieves notes on a player
func (c *Client) GetNotes(player string, page, notesPerPage int) (result *Notes, err error) {
	result = &Notes{}
	err = c.getAndUnmarshal(result, player, strconv.Itoa(page), strconv.Itoa(notesPerPage))
	if err != nil {
		return
	}
	return
}

// GetNoteCount retrieves count of notes for player
func (c *Client) GetNoteCount(player string) (notes int, err error) {
	type noteCountResponse struct {
		Count   int  `json:"totalcount"`
		Success bool `json:"success"`
	}
	n := &noteCountResponse{}
	err = c.getAndUnmarshal(n, player)
	if err != nil {
		return
	}

	if n.Success {
		notes = n.Count
	}
	return
}
