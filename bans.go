package mcbouncer

import (
	"strconv"
	"time"
)

// TimeFormat is the time format
const TimeFormat = `"2006-01-02T15:04:05.999"`

// BanResult is returned from AddBan
type BanResult struct {
	User    string `json:"user"`
	Success bool   `json:"success"`
	Error   string `json:"error"`
}

// AddBan submits a ban to the api
func (c *Client) AddBan(issuer, player, reason string) (result *BanResult, err error) {
	result = &BanResult{}
	err = c.getAndUnmarshal(result, AddBan, issuer, player, reason)
	if err != nil {
		return
	}
	return
}

// RemoveBan removes player ban
func (c *Client) RemoveBan(player string) (result *BanResult, err error) {
	result = &BanResult{}
	err = c.getAndUnmarshal(result, RemoveBan, player)
	if err != nil {
		return
	}
	return
}

// Bans holds information on bans
type Bans struct {
	Count   int          `json:"totalcount"`
	Page    int          `json:"page"`
	Success bool         `json:"success"`
	Bans    []*BanDetail `json:"data"`
}

// BanDetail holds information on a specific ban
type BanDetail struct {
	Username string `json:"username"`
	IP       string `json:"ip"`
	Reason   string `json:"reason"`
	Server   string `json:"server"`
	Time     *Time  `json:"time"`
	Issuer   string `json:"issuer"`
	Banned   bool   `json:"is_banned"`
}

// Time holds time type
type Time struct {
	time.Time
}

// MarshalJSON implements MarshalJSON for custom ban timestamp
func (bt *Time) MarshalJSON() ([]byte, error) {
	return []byte(bt.Format(TimeFormat)), nil
}

// UnmarshalJSON implements UnmarshalJSON for custom ban timestamp
func (bt *Time) UnmarshalJSON(b []byte) (err error) {
	ts, err := time.Parse(TimeFormat, string(b))
	if err != nil {
		return
	}
	*bt = Time{ts}
	return
}

// GetBan retrieves bans for a player
func (c *Client) GetBan(player string, page, bansPerPage int) (result *Bans, err error) {
	result = &Bans{}
	err = c.getAndUnmarshal(result, GetBans, player, strconv.Itoa(page), strconv.Itoa(bansPerPage))
	if err != nil {
		return
	}
	return
}

// GetBanCount returns ban count for player
func (c *Client) GetBanCount(player string) (bans int, err error) {
	result := &Bans{}
	err = c.getAndUnmarshal(result, GetBanCount, player)
	if err != nil {
		return
	}

	bans = result.Count
	return
}

// GetBanReason retrieves reason for specified player's ban on your server
func (c *Client) GetBanReason(player string) (result *BanDetail, err error) {
	result = &BanDetail{}
	err = c.getAndUnmarshal(result, GetBanReason, player)
	if err != nil {
		return
	}
	return
}

// GetBanDetails retrieves details on ban for specified player's ban on your server
func (c *Client) GetBanDetails(player string) (result *BanDetail, err error) {
	result = &BanDetail{}
	err = c.getAndUnmarshal(result, GetBanDetails, player)
	if err != nil {
		return
	}
	return
}

// IPBan holds details about an IP ban
type IPBan struct {
	IPAddress string `json:"ipaddress"`
	Success   bool   `json:"success"`
	Error     string `json:"error"`
}

// AddIPBan adds IP ban
func (c *Client) AddIPBan(issuer, ip, reason string) (result *IPBan, err error) {
	result = &IPBan{}
	err = c.getAndUnmarshal(result, AddIPBan, issuer, ip, reason)
	if err != nil {
		return
	}
	return
}

// GetIPBan retrieves bans for provided IP address
func (c *Client) GetIPBan(ip string, page, bansPerPage int) (result *Bans, err error) {
	result = &Bans{}
	err = c.getAndUnmarshal(result, GetIPBans, ip, strconv.Itoa(page), strconv.Itoa(bansPerPage))
	if err != nil {
		return
	}
	return
}

// RemoveIPBan removes ban for provided IP address
func (c *Client) RemoveIPBan(ip string) (result *IPBan, err error) {
	result = &IPBan{}
	err = c.getAndUnmarshal(result, RemoveIPBan, ip)
	if err != nil {
		return
	}
	return
}

// GetIPBanCount returns ban count for provided IP address
func (c *Client) GetIPBanCount(ip string) (count int, err error) {
	result := &Bans{}
	err = c.getAndUnmarshal(result, GetBanCount, ip)
	if err != nil {
		return
	}

	count = result.Count
	return
}
