package mcbouncer

import (
	"fmt"
	"log"
	"net/url"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestBan(t *testing.T) {
	server := newTestServer()
	defer server.Close()

	baseURL, err := url.Parse(server.URL)
	if err != nil {
		log.Fatalln(err)
	}
	client := &Client{
		apiKey:  testAPIKey,
		apiURLf: fmt.Sprintf("%s/%s", baseURL.String(), testAPIPathf),
	}

	Convey("Test Bans API", t, func() {
		Convey("Add Ban", func() {
			banResult, addBanErr := client.AddBan(testIssuer, testUser, testDescription)
			So(addBanErr, ShouldBeNil)
			So(banResult, ShouldNotBeNil)
		})

		Convey("Remove Ban", func() {
			removeBanResult, removeBanErr := client.RemoveBan(testUser)
			So(removeBanErr, ShouldBeNil)
			So(removeBanResult, ShouldNotBeNil)
		})

		Convey("Get Ban", func() {
			bans, getBanErr := client.GetBan(testUser, testPageInt, testResultsPerPageInt)
			So(getBanErr, ShouldBeNil)
			So(bans, ShouldNotBeNil)
		})

		Convey("Get Ban Count", func() {
			banCount, getBanCountErr := client.GetBanCount(testUser)
			So(getBanCountErr, ShouldBeNil)
			So(banCount, ShouldEqual, 2)
		})

		Convey("Get Ban Reason", func() {
			banReason, getBanReasonErr := client.GetBanReason(testUser)
			So(getBanReasonErr, ShouldBeNil)
			So(banReason, ShouldNotBeNil)
		})

		Convey("Get Ban Details", func() {
			banDetails, getBanDetailsErr := client.GetBanDetails(testUser)
			So(getBanDetailsErr, ShouldBeNil)
			So(banDetails, ShouldNotBeNil)
		})

		Convey("Add IP Ban", func() {
			ipBan, addIPBanErr := client.AddIPBan(testIssuer, testIP, testDescription)
			So(addIPBanErr, ShouldBeNil)
			So(ipBan, ShouldNotBeNil)
		})
	})
}
