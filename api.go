package mcbouncer

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"path"
)

// APIURLf is the format string for the api url
// /api/{action}/{apikey}/{params}
// params are in the format `param0/param1/param2`
const APIURLf = "http://www.mcbouncer.com/api/%s/%s/%s"

// actions
const (
	AddBan        = "addBan"
	RemoveBan     = "removeBan"
	GetBans       = "getBans"
	GetBanCount   = "getBanCount"
	GetBanReason  = "getBanReason"
	GetBanDetails = "getBanDetails"
	AddIPBan      = "addIPBan"
	RemoveIPBan   = "removeIPBan"
	GetIPBans     = "getIPBans"
	GetIPBanCount = "getIPBanCount"
	AddNote       = "addNote"
	RemoveNote    = "removeNote"
	GetNotes      = "getNotes"
	GetNoteCount  = "getNoteCount"
	UpdateUser    = "updateUser"
)

// Client implements mcbouncer api
type Client struct {
	apiKey  string
	apiURLf string
}

// NewClient returns a new client
func NewClient(apiKey string) *Client {
	return &Client{
		apiKey:  apiKey,
		apiURLf: APIURLf,
	}
}

func (c *Client) urlf(action string, params ...string) (u string) {
	for i, v := range params {
		params[i] = url.QueryEscape(v)
	}

	u = fmt.Sprintf(c.apiURLf, action, c.apiKey, path.Join(params...))
	return
}

func (c *Client) getAndUnmarshal(dst interface{}, action string, params ...string) (err error) {
	url := c.urlf(action, params...)

	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("error requesting url: %s", err.Error())
	}
	defer resp.Body.Close()

	if resp.StatusCode == 404 {
		return
	}

	if resp.StatusCode != 200 {
		err = errors.New("mcbouncer: received non-200 response")
		return
	}

	err = json.NewDecoder(resp.Body).Decode(dst)
	if err != nil {
		return fmt.Errorf("error decoding response: %s", err.Error())
	}

	return nil
}
